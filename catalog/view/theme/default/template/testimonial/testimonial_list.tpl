<?php if ($reviews) { ?>
<style>
    .table-testimonials, .table-testimonials tr, .table-testimonials td {
        border: none !important;
        font-size: 14px;
    }

    .table-testimonials .table-testimonial-td {
        background: #f9f9f9;
        padding: 10px 30px 10px 30px;
        font-size: 14px;
    }
</style>
<div class="row">
    <?php foreach ($reviews as $review) { ?>
    <div class="col-xs-12 col-md-6">
        <table class="table table-striped table-bordered table-testimonials">
            <tr>
                <td style="width: 50%; padding: 10px 30px 10px 30px;">
                    <strong><?php echo $review['author']; ?></strong>
                </td>
                <td class="text-right" style="padding: 10px 30px 10px 30px;"><?php echo $review['date_added']; ?></td>
            </tr>
            <tr>
                <td class="table-testimonial-td" colspan="2"><p><?php echo $review['text']; ?></p>
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($review['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x" style='color: #FC0;'></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x" style='color: #FC0;'></i><i
                                class="fa fa-star-o fa-stack-2x" style='color: #E69500;'></i></span>
                    <?php } ?>
                    <?php } ?>
                </td>
            </tr>
        </table>
    </div>
    <?php } ?>
</div>
<div class="row">
    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
