<div class="dropdown dp-style">
    <a id="phonenumbertrigger" class="dropdown-toggle pull-left" data-toggle="dropdown" href="#"><i class="fa fa-phone fs-18 mr-5"></i><div class="displaynonetabletka"><?=$telephones[0]['telephone'];?></div></a>
    <ul class="dropdown-menu telephone-contacts" style=" margin-top: 3em;box-shadow: -2px 2px 18px rgba(68, 68, 68, 0.15);">
        <?php foreach($telephones as $telephone) { ?>
        <li class="col-sm-12 vertical-center " style="padding: 20px 25px 20px 15px;">
            <div class="col-sm-2" style="padding-right:0;">
                <i class="fa fa-map-marker fs-24"></i>
            </div>
            <div class="col-sm-10" style="padding-left:0;">
            <a class="fw-600 mb-10" style="display:block;" href="#"><?=$telephone['sity']?></a>
            <a class="dropdown-text color666 fs-14" style="line-height:18px;display:block;" href="#"><?=$telephone['telephone']?></a>
            <a class="dropdown-text color666 fs-14" style="line-height:18px;display:block;" href="#"><?=$telephone['address']?></a>
            </div>
        </li>
        <?php } ?>
    </ul>
</div>