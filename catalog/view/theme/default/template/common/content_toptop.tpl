<div class="container-full-width">
    <div id="toptop" class="row mb-80">
        <?php $i = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <?php if($i==0){ ?>
        <div id="slider-top" >
            <div class="col-sm-12 col-md-12 col-lg-8">
                <?php echo $module; ?>
            </div>
        </div>
        <?php } else { ?>
        <div id="contact-top">
            <div class="col-sm-12 col-md-12 col-lg-4 contact-form-top-col">
                <?php echo $module; ?>
            </div>
        </div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
    </div>
</div>