<?php echo $header; ?>
<ul class="breadcrumb">     <div class="breadcrumb-header"><?php echo $heading_title; ?></div>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
</ul>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <!--
            <h2><?php echo $heading_title; ?></h2>
            <?php if ($thumb || $description) { ?>
            <div class="row">
                <?php if ($thumb) { ?>
                <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"
                                           title="<?php echo $heading_title; ?>" class="img-thumbnail"/></div>
                <?php } ?>
                <?php if ($description) { ?>
                <div class="col-sm-10"><?php echo $description; ?></div>
                <?php } ?>
            </div>
            <hr>
            <?php } ?>
            <?php if ($categories) { ?>
            <h3><?php echo $text_refine; ?></h3>
            <?php if (count($categories) <= 5) { ?>
            <div class="row">
                <div class="col-sm-3">
                    <ul>
                        <?php foreach ($categories as $category) { ?>
                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php } else { ?>
            <div class="row">
                <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
                <div class="col-sm-3">
                    <ul>
                        <?php foreach ($categories as $category) { ?>
                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
            <?php } ?>
            -->
            <style>
                .compare-products{
                    min-width: 171px;
                    font-size: 14px;
                    font-weight: 400;
                    color: #333333;
                }
                .sort-products{
                    min-width: 90px;
                    font-size: 14px;
                    font-weight: 400;
                    color: #333333;
                    padding: 0;
                    margin: 0;
                    margin-left: auto;
                }
                .count-view-products{
                    min-width: 75px;
                    font-size: 14px;
                    font-weight: 400;
                    color: #333333;
                    padding: 0;
                    margin: 0;
                    margin-left: auto;
                }
                .count-view-products-select{
                    max-width: 150px;
                }
            </style>
            <?php if ($products) { ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-8 vertical-center">
                       <!-- <a class="compare-products hidden-xs" href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a>-->
                        <label class="control-label sort-products" for="input-sort"><?php echo $text_sort; ?></label>
                        <select id="input-sort" class="form-control count-view-products-select" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                            <option value="<?php echo $sorts['href']; ?>"
                                    selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-4 vertical-center">

                        <label class="control-label count-view-products" for="input-limit"><?php echo $text_limit; ?></label>
                        <select id="input-limit" class="form-control count-view-products-select" onchange="location = this.value;">
                            <?php foreach ($limits as $limits) { ?>
                            <?php if ($limits['value'] == $limit) { ?>
                            <option value="<?php echo $limits['href']; ?>"
                                    selected="selected"><?php echo $limits['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>

                </div>

                <!--
                <div class="col-md-4 text-right">
                    <a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a>
                    <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                </div>
                <div class="col-md-2 pull-left">
                    <select id="input-sort" class="form-control" onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) { ?>
                        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <option value="<?php echo $sorts['href']; ?>"
                                selected="selected"><?php echo $sorts['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3 text-right">

                </div>
                <div class="col-md-1 text-right">
                    <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                </div>
                <div class="col-md-2 text-right">
                    <select id="input-limit" class="form-control" onchange="location = this.value;">
                        <?php foreach ($limits as $limits) { ?>
                        <?php if ($limits['value'] == $limit) { ?>
                        <option value="<?php echo $limits['href']; ?>"
                                selected="selected"><?php echo $limits['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                -->
            </div>
            <br/>
            <div id="products" class="row">
                <?php foreach ($products as $product) { ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="product-thumb transition popular-product" style="border: none;">
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"
                                                                                          alt="<?php echo $product['name']; ?>"
                                                                                          title="<?php echo $product['name']; ?>"
                                                                                          class="img-responsive"/></a></div>
                        <div class="caption" style="min-height: 25px;">
                            <p class="text-center h4"><a class="text f-14" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            </p>
                            <!--<   p><?php echo $product['description']; ?></p>-->
                            <?php if ($product['rating']) { ?>
                            <div class="rating">
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($product['rating'] < $i) { ?>
                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } else { ?>
                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
                                            class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <?php if ($product['price']) { ?>
                            <p class="price text-center">
                                <?php if (!$product['special']) { ?>
                                <?php echo $product['price']; ?>
                                <?php } else { ?>
                                <span class="price-new"><?php echo $product['special']; ?></span> <span
                                        class="price-old"><?php echo $product['price']; ?></span>
                                <?php } ?>
                                <?php if ($product['tax']) { ?>
                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                <?php } ?>
                            </p>
                            <?php } ?>
                        </div>
                        <a class="btn width-100-p btn-go-more" href="<?php echo $product['href']; ?>"><?php echo $button_cart; ?></a>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="row vertical-center" style="min-width: 100%">
                <div class="col-sm-4 hidden-xs text-left"><?php echo $results; ?></div>
                <div class="col-sm-8 col-xs-12 text-right"><?php echo $pagination; ?></div>
            </div>

            <?php } ?>

            <?php if (!$categories && !$products) { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>"
                                           class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php } ?>
            <div>
                <?php if ($description) { ?>
                <div style="margin-top: 50px;"><?php echo $description; ?></div>
                <?php } ?>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>

</div>
<div class="container">
   <?php echo $content_nadfutercont; ?>
</div>

<?php echo $footer; ?>
