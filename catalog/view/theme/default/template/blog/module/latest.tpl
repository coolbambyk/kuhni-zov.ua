<div class="h3 text-center pb-30"><?php echo $heading_title; ?></div>
<div class="mb-60 display-table">
    <?php foreach ($articles as $article) { ?>
    <div class="product-layout col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="product-thumb transition" style="border: none;">
            <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>"
                                                                              alt="<?php echo $article['name']; ?>"
                                                                              title="<?php echo $article['name']; ?>"
                                                                              class="img-responsive"/></a></div>
            <div class="caption">
                <p class="h4"><a class="text-uppercase" style="color: #000;font-weight: 500; line-height: 6.51px; letter-spacing: 0.64px;" href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></p>
                <p style="color: #999999;" class="f-14"><?php echo $article['description']; ?></p>
            </div>
            <a href="<?php echo $article['href']; ?>" class="f-14 pl-20 pr-20"
               style="display: block;"><?php echo $button_more; ?> <span class="lnr lnr-chevron-right f-10"></span></a>
        </div>
    </div>
    <?php } ?>
</div>