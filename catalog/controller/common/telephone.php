<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCommonTelephone extends Controller {
    public function index() {
        $this->load->model('localisation/telephone');
        $data['telephones'] = $this->model_localisation_telephone->getTelephones();
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/telephone.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/common/telephone.tpl', $data);
        } else {
            return $this->load->view('default/template/common/telephone.tpl', $data);
        }
    }
}