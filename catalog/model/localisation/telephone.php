<?php
class ModelLocalisationTelephone extends Model
{
    public function getTelephones()
    {
        $language_data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "telephone WHERE language_id = {$this->config->get('config_language_id')} ORDER BY sort_order, sity");

        foreach ($query->rows as $result) {
            $language_data[] = array(
                'language_id' => $result['language_id'],
                'telephone' => $result['telephone'],
                'sity' => $result['sity'],
                'address' => $result['address'],
                'sort_order' => $result['sort_order'],
            );
        }
        return $language_data;
    }
}