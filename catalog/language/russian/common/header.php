<?php
$_['text_home'] = 'Главная';
$_['text_wishlist'] = 'Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category'] = 'Категории';
$_['text_account'] = 'Личный кабинет';
$_['text_register'] = 'Регистрация';
$_['text_login'] = 'Авторизация';
$_['text_order'] = 'История заказа';
$_['text_transaction'] = 'Операции';
$_['text_download'] = 'Файлы для скачивания';
$_['text_logout'] = 'Выход';
$_['text_checkout'] = 'Оформление заказа';
$_['text_search'] = 'Поиск';
$_['text_all'] = 'Смотреть Все';
$_['text_logo_bottom'] = 'Белорусская фабрика ЗОВ в Украине';
$_['text_logo_top'] = 'Кухни под заказ';
?>