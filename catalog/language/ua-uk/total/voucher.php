<?php
$_['heading_title'] = 'Використати Подарунковий сертифікат';
$_['text_voucher'] = 'Voucher (%s)';
$_['text_success'] = 'Success: Your gift voucher discount has been applied!';
$_['entry_voucher'] = 'Enter your gift voucher code here';
$_['error_voucher'] = 'Warning: Gift Voucher is either invalid or the balance has been used up!';
$_['error_empty'] = 'Warning: Please enter a voucher code!';
?>