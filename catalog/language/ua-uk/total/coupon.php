<?php
$_['heading_title'] = 'Використати купон';
$_['text_coupon'] = 'Coupon (%s)';
$_['text_success'] = 'Success: Your coupon discount has been applied!';
$_['entry_coupon'] = 'Enter your coupon here';
$_['error_coupon'] = 'Warning: Coupon is either invalid, expired or reached its usage limit!';
$_['error_empty'] = 'Warning: Please enter a coupon code!';
?>