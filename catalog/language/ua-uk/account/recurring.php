<?php
$_['heading_title'] = 'Періодичні платежі';
$_['text_account'] = 'Запис';
$_['text_recurring'] = 'Періодичні платежі';
$_['text_recurring_detail'] = 'Деталі періодичного платежу';
$_['text_order_recurring_id'] = '№ ';
$_['text_date_added'] = 'Створено:';
$_['text_status'] = 'Статус';
$_['text_payment_method'] = 'Спосіб оплати:';
$_['text_order_id'] = '№ замовлення:';
$_['text_product'] = 'Товар:';
$_['text_quantity'] = 'Кількість:';
$_['text_description'] = 'Опис';
$_['text_reference'] = 'Зв&#39;язок';
$_['text_transaction'] = 'Транзакції';
$_['text_status_1'] = 'Активний';
$_['text_status_2'] = 'Неактивний';
$_['text_status_3'] = 'Анульований';
$_['text_status_4'] = 'Заморожений';
$_['text_status_5'] = 'Минув';
$_['text_status_6'] = 'Очікує';
$_['text_transaction_date_added'] = 'Створено';
$_['text_transaction_payment'] = 'Оплата';
$_['text_transaction_outstanding_payment'] = 'Несплачений платіж';
$_['text_transaction_skipped'] = 'Пропущений платіж';
$_['text_transaction_failed'] = 'Помилка платежу';
$_['text_transaction_cancelled'] = 'Скасований';
$_['text_transaction_suspended'] = 'Заморожений';
$_['text_transaction_suspended_failed'] = 'Заморожений за помилки оплати';
$_['text_transaction_outstanding_failed'] = 'Несплачений через помилки оплати';
$_['text_transaction_expired'] = 'Минув';
$_['text_empty'] = 'Періодичні платежі не знайдено';
$_['text_error'] = 'Періодичні замовлення не знайдено!';
$_['text_cancelled'] = 'Періодичний платіж був скасований';
$_['column_date_added'] = 'Створено';
$_['column_type'] = 'Тип';
$_['column_amount'] = 'Разом';
$_['column_status'] = 'Статус';
$_['column_product'] = 'Товар';
$_['column_action'] = 'Дія';
$_['column_recurring_id'] = 'ID Профілю';
$_['error_not_cancelled'] = 'Помилка: %s';
$_['error_not_found'] = 'не Можна відключити профіль';
$_['button_return'] = 'Повернення';
$_['button_continue'] = 'Продовжити';
$_['button_view'] = 'Перегляд';
$_['text_order'] = 'Замовленя';
$_['text_action'] = 'Дія';
$_['text_transactions'] = 'Транзакції';
$_['text_empty_transactions'] = 'Нема транзакцій для цього профілю';
$_['text_recurring_id'] = 'ID Профіля';
$_['text_recurring_description'] = 'Опис';
$_['text_ref'] = 'Сноска';
$_['text_status_active'] = 'Активний';
$_['text_status_inactive'] = 'Неактивний';
$_['text_status_cancelled'] = 'Анульований';
$_['text_status_suspended'] = 'Заморожений';
$_['text_status_expired'] = 'Закінчився';
$_['text_status_pending'] = 'Очікує';
?>