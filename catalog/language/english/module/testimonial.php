<?php
// Text
$_['heading_title']	           = 'Отзывы';

$_['text_write']               = 'Написать отзыв';
$_['text_login']               = 'Пожалуйста <a href="%s">залогинтесь</a> или <a href="%s">зареестрируйтесь</a> для написаня отзыва';
$_['text_no_reviews']          = 'Нету отзывов.';
$_['text_note']                = '<span class="text-danger">Внимание:</span> HTML не переводится!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';

$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Author: %s';
$_['text_mail_rating']	       = 'Rating: %s';
$_['text_mail_text']	       = 'Text:';

// Entry
$_['entry_name']               = 'Имя';
$_['entry_review']             = 'Отзыв';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Хорошо';
$_['entry_bad']                = 'Плохо';

// Button
$_['button_continue']          = 'Продолжить';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 3000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';
$_['error_captcha']            = 'Warning: Verification code does not match the image!';

