<?php
// *	@copyright	OPENCART.PRO 2011 - 2015.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerLocalisationTelephone extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('localisation/telephone');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/telephone');
		$this->load->model('localisation/language');

		$this->getList();
	}

	public function add() {
		$this->load->language('localisation/telephone');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/telephone');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_localisation_telephone->addTelephone($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			$this->response->redirect($this->url->link('localisation/telephone', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('localisation/telephone');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/telephone');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_localisation_telephone->editTelephone($this->request->get['telephone_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			$this->response->redirect($this->url->link('localisation/telephone', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('localisation/telephone');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/telephone');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $telephone_id) {
				$this->model_localisation_telephone->deleteTelephone($telephone_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';


			$this->response->redirect($this->url->link('localisation/telephone', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function refresh() {
		$this->load->language('localisation/telephone');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('localisation/telephone');

		if ($this->validateRefresh()) {
			$this->model_localisation_telephone->refresh(true);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			$this->response->redirect($this->url->link('localisation/telephone', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {



		$url = '';
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('localisation/telephone', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('localisation/telephone/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('localisation/telephone/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['refresh'] = $this->url->link('localisation/telephone/refresh', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['telephones'] = array();


		$currency_total = $this->model_localisation_telephone->getTotalTelephones();

		$results = $this->model_localisation_telephone->getTelephones();
        $languages = $this->model_localisation_language->getLanguages();

		foreach ($results as $result) {
		     $lan = array();
		    foreach ($languages as $language){
                if ($result['language_id'] == $language['language_id']){
                    $lan = $language;
                    break;
                }
            }
			$data['telephones'][] = array(
				'telephone_id'   => $result['telephone_id'],
				'language'      => $lan,
				'language_id'   => $result['language_id'],
				'sity'         => $result['sity'],
				'address'          => $result['address'],
				'telephone'         => $result['telephone'],
				'edit'          => $this->url->link('localisation/telephone/edit', 'token=' . $this->session->data['token'] . '&telephone_id=' . $result['telephone_id'] . $url, 'SSL')
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_title'] = $this->language->get('column_title');
		$data['column_code'] = $this->language->get('column_code');
		$data['column_value'] = $this->language->get('column_value');
		$data['column_date_modified'] = $this->language->get('column_date_modified');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_telephone'] = $this->language->get('button_telephone');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('localisation/telephone_list.tpl', $data));
	}

	protected function getForm() {
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['currency_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_code'] = $this->language->get('entry_code');
		$data['entry_value'] = $this->language->get('entry_value');
		$data['entry_symbol_left'] = $this->language->get('entry_symbol_left');
		$data['entry_symbol_right'] = $this->language->get('entry_symbol_right');
		$data['entry_decimal_place'] = $this->language->get('entry_decimal_place');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['help_code'] = $this->language->get('help_code');
		$data['help_value'] = $this->language->get('help_value');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = '';
		}

		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		$url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('localisation/telephone', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (!isset($this->request->get['telephone_id'])) {
			$data['action'] = $this->url->link('localisation/telephone/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('localisation/telephone/edit', 'token=' . $this->session->data['token'] . '&telephone_id=' . $this->request->get['telephone_id'] . $url, 'SSL');
		}

		$data['cancel'] = $this->url->link('localisation/telephone', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['telephone_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$currency_info = $this->model_localisation_telephone->getTelephone($this->request->get['telephone_id']);
		}

		if (isset($this->request->post['sity'])) {
			$data['sity'] = $this->request->post['sity'];
		} elseif (!empty($currency_info)) {
			$data['sity'] = $currency_info['sity'];
		} else {
			$data['sity'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} elseif (!empty($currency_info)) {
			$data['telephone'] = $currency_info['telephone'];
		} else {
			$data['telephone'] = '';
		}

		if (isset($this->request->post['address'])) {
			$data['address'] = $this->request->post['address'];
		} elseif (!empty($currency_info)) {
			$data['address'] = $currency_info['address'];
		} else {
			$data['address'] = '';
		}

		if (isset($this->request->post['language_id'])) {
			$data['language_id'] = $this->request->post['language_id'];
		} elseif (!empty($currency_info)) {
			$data['language_id'] = $currency_info['language_id'];
		} else {
			$data['language_id'] = '';
		}

        $data['languages'] = $languages;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('localisation/telephone_form.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'localisation/telephone')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	protected function validateDelete() {
		return !$this->error;
	}

	protected function validateRefresh() {
		if (!$this->user->hasPermission('modify', 'localisation/telephone')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}