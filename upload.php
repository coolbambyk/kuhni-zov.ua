<?php
ini_set('display_errors','Off');
header("Content-Type: text;charset=utf-8");
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_FILES['files'])) {
        $errors = [];
        $path = 'files/';


        $all_files = count($_FILES['files']['tmp_name']);

        for ($i = 0; $i < $all_files; $i++) {
            $file_name = $_FILES['files']['name'][$i];
            $file_tmp = $_FILES['files']['tmp_name'][$i];
            $file_type = $_FILES['files']['type'][$i];
            $file_size = $_FILES['files']['size'][$i];
            $file_ext = strtolower(end(explode('.', $_FILES['files']['name'][$i])));

//            $value = explode(".", $_FILES['files']['name'][$i]);
//            $extension = strtolower(array_pop($value));   //Line 32
//            // the file name is before the last "."
//            $file_ext = array_shift($value);  //Line 34

            $file = $path .rand(2,99999999). $file_name;



            if ($file_size > 2097152) {
                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
            }

            if (empty($errors)) {
                move_uploaded_file($file_tmp, $file);
            }
        }

        if ($errors)
            {print_r($errors);}
        else
            {echo $file;};
    }
}
ini_set('display_errors','On');
error_reporting('E_ALL');
?>